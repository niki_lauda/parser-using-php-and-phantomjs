var page = require("webpage").create();
var url = "http://www.sportsporudy.gov.ua/catalog/#c[1]=1";

function save(fileName, content){
  var fs = require('fs');
  try{
    fs.write(fileName, content, 'w');
  }
  catch(e){
    console.log(e);
  }
  phantom.exit();
}


function onPageReady(){
  var content = page.evaluate(function(){
    return document.documentElement.innerHTML;
  });
  console.log(JSON.stringify(content));
  phantom.exit();
}

page.open(url, function(status){
  function checkReadyState(){
    setTimeout(function(){
      var readyState = page.evaluate(function(){
        return document.readyState;
      });
      if('complete' === readyState){
        onPageReady();
      }
      else{
        checkReadyState();
      }
    })
  }
  checkReadyState();
});
